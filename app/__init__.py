from flask import Flask
from flask_restful import Api
from app.config import Config


app = Flask(__name__)
app.config.from_object(Config)
api = Api(app)
# migrate = Migrate(app, db)

# from src.resources.index import IndexApi
# from src.resources.cities import CitylistApi
#
#
# api.add_resource(IndexApi, "/", '/index', strict_slashes=False)
# api.add_resource(CitylistApi, "/cities", strict_slashes=False)
