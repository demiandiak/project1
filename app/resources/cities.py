from flask_restful import Resource
from schemas.cities import CitySchema


class CitylistApi(Resource):
    city_schema = CitySchema()

    def get(self):
        return {'Cities': 200}
