from app import api
from resources.index import IndexApi
from resources.cities import CitylistApi


api.add_resource(IndexApi, "/", '/index', strict_slashes=False)
api.add_resource(CitylistApi, "/cities", strict_slashes=False)
