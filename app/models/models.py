from datetime import datetime as dt
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Text, LargeBinary, Boolean
from sqlalchemy.orm import declarative_base, relationship, scoped_session, sessionmaker
from sqlalchemy import create_engine
from app.config import Config
from sqlalchemy_utils import database_exists, create_database


engine = create_engine(Config.SQLALCHEMY_DATABASE_URI, echo=True)
Base = declarative_base()
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String(120), nullable=False, unique=True)
    password = Column(String(120), nullable=False)
    name = Column(String(30), nullable=False)
    sname = Column(String(30))
    gender = Column(String(6))
    avatar = Column(LargeBinary)
    created = Column(DateTime)
    admin = Column(Boolean, nullable=False)
    comments = relationship('Comments', back_populates="users")
    responds = relationship('Responds', back_populates="users")


class City(Base):
    __tablename__ = 'cities'

    id = Column(Integer, primary_key=True)
    slug = Column(String(140), unique=True)
    name = Column(String(30), nullable=False)
    country = Column(String(30), nullable=False)
    region = Column(String(30))
    district = Column(String(30))
    kind = Column(String(30))

    def __repr__(self):
        return f'City({self.name, self.country})'


class Comment(Base):
    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True)
    created = Column(DateTime, default=dt.now())
    user_id = Column(Integer, ForeignKey('users.id'))
    text = Column(Text, nullable=False)

    users = relationship('Users', back_populates="comments")


class Respond(Base):
    __tablename__ = 'responds'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    created = Column(DateTime, default=dt.now())
    text = Column(Text, nullable=False)
    users = relationship('Users', back_populates="responds")


def validate_database():
    if not database_exists(engine.url):  # Checks for the first time
        create_database(engine.url)      # Create new DB
        print("New Database Created")    # Verifies if database is there or not.
    else:
        print("Database Already Exists")
    Base.metadata.create_all(engine)


if __name__ == '__main__':
    validate_database()
