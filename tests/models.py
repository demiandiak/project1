from datetime import datetime as dt
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Text, LargeBinary, Boolean
from sqlalchemy.orm import declarative_base, sessionmaker, relationship
from app_old_version.config import DB_URI
from flask_sqlalchemy import Model
Base = declarative_base()


DATABSE_URI = '{mode}://{user}:{password}@{server}:{port}/{database}'.format(**DB_URI)
engine = create_engine(DATABSE_URI, echo=True)
session = sessionmaker(bind=engine)


class Users(Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String(120), nullable=False, unique=True)
    password = Column(String(120), nullable=False)
    name = Column(String(30), nullable=False)
    sname = Column(String(30))
    gender = Column(String(6))
    avatar = Column(LargeBinary)
    created = Column(DateTime)
    admin = Column(Boolean, nullable=False)
    comments = relationship('Comments', back_populates="users")
    responds = relationship('Responds', back_populates="users")


class Cities(Model):
    __tablename__ = 'cities'

    id = Column(Integer, primary_key=True)
    slug = Column(String(140), unique=True)
    name = Column(String(30), nullable=False)
    country = Column(String(30), nullable=False)
    region = Column(String(30))
    district = Column(String(30))
    kind = Column(String(30))


class Comments(Model):
    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True)
    created = Column(DateTime, default=dt.now())
    user_id = Column(Integer, ForeignKey('users.id'))
    text = Column(Text, nullable=False)

    users = relationship('Users', back_populates="comments")


class Responds(Model):
    __tablename__ = 'responds'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    created = Column(DateTime, default=dt.now())
    text = Column(Text, nullable=False)
    users = relationship('Users', back_populates="responds")


if __name__ == '__main__':
    Base.metadata.create_all(engine)
