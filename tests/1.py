"""this file contains app_old_version and routes"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app_old_version.config import Config
from flask_migrate import Migrate
from flask_restful import Api


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)


from app_old_version.src.routes import Index, City

api.add_resource(Index, '/', strict_slashes=False)
api.add_resource(City, '/cities', '/cities/<slug>', strict_slashes=False)

# from flask_swagger_ui import get_swaggerui_blueprint
# SWAGGER_URL = '/swagger'
# API_URL = '/static/swagger.json'
# SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
#     SWAGGER_URL,
#     API_URL,
#     config={
#         'app_name': 'Flask tutorial'
#     }
# )
# app_old_version.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
