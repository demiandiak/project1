from flask_restful import Resource
from app_old_version.src import db
from app_old_version.src.models import Cities


class Index(Resource):
    def get(self):
        return {'message': 'OK'}


class City(Resource):
    'обробник запросів по містах'
    def get(self, slug=None):
        'return lists of cities of one city for id'
        if not slug:
            cities = db.session.query(Cities).all()
            return [city.to_dict() for city in cities], 200
        city = db.session.query(Cities).filter_by(slug=slug).first()
        if not city:
            return city.to_dict(), 200
        return "City not found", 404

    def post(self, id):
        pass

    def put(self, id):
        pass

    def delete(self, id):
        pass



